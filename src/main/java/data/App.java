package data;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


/**
 * Hello world!
 *
 */
public class App 
{
    private static Logger logger = LogManager.getLogger(App.class);
    private final String LINK = "https://www.fcupdate.nl/voetbaluitslagen/286898/wk-kwalificatie-europa-ierland-denemarken/";

    public static void main( String[] args )
    {
        logger.info("Started app.");
        App app = new App();
        app.init();
    }

    private void init() {
        logger.trace("Entered");

        MySQLUtils mySQLUtils = new MySQLUtils();

        Match match = new Match(LINK, mySQLUtils);
        logger.info("Getting data for {} - {}", match.getHomeTeamName(), match.getAwayTeamName());

        logger.trace("Out");
    }

}
