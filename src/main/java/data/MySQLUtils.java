package data;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

public class MySQLUtils {

    private Connection conn = null;
    private Logger logger = LogManager.getLogger(MySQLUtils.class);
    private String user, pass, host, db;

    MySQLUtils() {

        logger.trace("In constructor");
        readSqlConfig();
        createConnection();
    }

    private void readSqlConfig() {
        logger.trace("In readSqlConfig()");
        Properties props = new Properties();
        try {
            FileInputStream inputStream = new FileInputStream("config/mysql.properties");

            props.load(inputStream);
            user = props.getProperty("user");
            pass = props.getProperty("password");
            host = props.getProperty("url");
            db = props.getProperty("database");
        } catch (IOException e) {
            logger.error(e.getMessage());
            System.exit(-1);
        }
    }

    private void createConnection() {
        logger.trace("In createConnection()");
        final String DRIVER = "com.mysql.cj.jdbc.Driver";
        try {
            Class.forName(DRIVER);
            conn = DriverManager.getConnection("jdbc:mysql://" + host + "?user=" + user + "&password=" + pass +
                    "&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");

        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage() + "\nShutting down");
            System.exit(-1);
        } catch (SQLException e) {
            logger.error(e.getMessage() + "\nShutting down");
            System.exit(-1);
        }
        logger.info("Successful SQL connection");
    }


    /*
      See below the sql queries
     */
    int getMatchId(String homeTeam, String awayTeam) throws SQLException {
        int matchId = 0;
        boolean foundMatchId = false;
        int homeId = getTeamId(homeTeam);
        int awayId = getTeamId(awayTeam);
        ResultSet homeMatches = getMatchesByTeam(homeId);
        ResultSet awayMatches = getMatchesByTeam(awayId);

        int matchIdColumn = homeMatches.findColumn("MatchID");

        while (homeMatches.next()) {
            int matchIdHome = homeMatches.getInt(matchIdColumn);
            System.out.println(homeMatches.getRow());
            while (awayMatches.next()) {
                int matchIdAway = awayMatches.getInt(matchIdColumn);
                if (matchIdHome == matchIdAway) {
                   matchId = matchIdHome;
                   foundMatchId = true;
                   break;
                }
                if (foundMatchId) {
                    break;
                }
            }
            if (!foundMatchId) {
                logger.error("Could not find matchID. Aborting...");
                System.exit(-1);
            }
        }
        logger.debug("Found match ID: " + matchId);
        return matchId;
    }

    ResultSet getMatchesByTeam(int countryId) throws SQLException {
        Statement stmt = conn.createStatement();
        String query = String.format("SELECT * FROM %s.Schedule WHERE `CountryID` = '%s'", db, countryId);
        return stmt.executeQuery(query);
    }

    int getTeamId(String teamName) throws SQLException {
        int id;
        Statement stmt = conn.createStatement();
        String query = String.format("SELECT `ID` FROM %s.Teams WHERE `Name` = '%s'", db, teamName);
        ResultSet rs = stmt.executeQuery(query);
        rs.next();
        id = rs.getInt(1);
        rs.close();
        return id;
    }
}
