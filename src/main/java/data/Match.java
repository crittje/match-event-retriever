package data;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

/**
 * Created by chrism on 8-1-18.
 */
public class Match {

    private Logger logger = LogManager.getLogger(Match.class);
    private String homeTeamName;
    private String awayTeamName;
    private int matchId;

    Match(final String LINK, final MySQLUtils mySQLUtils) {

        logger.debug("Creating initial match object");
        String[] splitter = LINK.split("-");
        int splitterLength = splitter.length;
        setHomeTeamName(splitter[splitterLength - 2]);
        setAwayTeamName(splitter[splitterLength - 1]);
        setAwayTeamName(getAwayTeamName().substring(0, getAwayTeamName().length() -1));
        try {
            matchId = mySQLUtils.getMatchId(getHomeTeamName(),getAwayTeamName());
        } catch (SQLException e) {
            logger.error(e.getMessage());
            System.exit(-1);
        }
    }

    private void getMatchId() {

    }

    public String getHomeTeamName() {
        return homeTeamName;
    }

    public void setHomeTeamName(String homeTeamName) {
        this.homeTeamName = homeTeamName;
    }

    public String getAwayTeamName() {
        return awayTeamName;
    }

    public void setAwayTeamName(String awayTeamName) {
        this.awayTeamName = awayTeamName;
    }
}
